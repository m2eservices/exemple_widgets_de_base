package com.example.jeanclaude.exemplewidgetsdebase

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // nouveau code avec Kotlin et kotlinX

        CheckBox01.setOnCheckedChangeListener { buttonView, isChecked ->
            afficheToast(
                "Case cochée ? : " + if (isChecked) "Oui" else "Non"
            )
        }

// ancien code converti automatiquement depuis Java
//        (findViewById<View>(R.id.CheckBox01) as CheckBox).setOnCheckedChangeListener(object :
//            CheckBox.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener {
//
//            override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
//                afficheToast("Case cochée ? : " + if (isChecked) "Oui" else "Non")
//            }
//        })


        (findViewById<View>(R.id.DatePicker01) as DatePicker).init(
            2018,
            10,
            29
        ) { view, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            afficheToast("La date a changé. Année : $year | Mois : $monthOfYear | Jour : $dayOfMonth")
        }


        // nouveau code Kotlin
        RatingBar01.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            afficheToast("Nouvelle note : $rating")
        }

        // ancien code Kotlin
//        (findViewById<View>(R.id.RatingBar01) as RatingBar).onRatingBarChangeListener =
//                RatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser ->
//                    afficheToast("Nouvelle note : $rating")
//                }

        // nouveau code Kotlin
        RadioGroup01.setOnCheckedChangeListener { group, checkedId ->
            afficheToast("Vous avez répondu : " + (findViewById<View>(checkedId) as RadioButton).text)
        }

        // ancien code Kotlin
//        (findViewById<View>(R.id.RadioGroup01) as RadioGroup).setOnCheckedChangeListener { group, checkedId ->
//            afficheToast("Vous avez répondu : " + (findViewById<View>(checkedId) as RadioButton).text)
//        }

        findViewById<View>(R.id.ImageButton01).setOnClickListener {
            // TODO Auto-generated method stub
            afficheToast("Bouton image cliqué")
        }

    }


    private fun afficheToast(text: String) {
        // TODO Auto-generated method stub
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }


}
